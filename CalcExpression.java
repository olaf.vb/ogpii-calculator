import java.util.ArrayList; 
public class CalcExpression
{
    private ArrayList<Float> operands;
    private ArrayList<Operator> operators;


    public enum Operator
    {
        SUBTRACTION,
        ADDITION,
        MULTIPLICATION,
        DIVISION;

        @Override
        public String toString()
        {
            switch(this)
            {
                case SUBTRACTION: return "-";
                case ADDITION: return "+";
                case MULTIPLICATION: return "*";
                case DIVISION: return "/";
                default: throw new IllegalArgumentException();
            }
        }

        public static Operator fromString(String s)
        {
            switch (s)
            {
                case "-": return SUBTRACTION;
                case "+": return ADDITION;
                case "*": return MULTIPLICATION;
                case "/": return DIVISION;
                default: throw new IllegalArgumentException();
            }
        }
    }


    public CalcExpression()
    {
        operands = new ArrayList<Float>();
        operators = new ArrayList<Operator>();
    }


    public void addOperand(float op)
    {
        operands.add(op);
    }


    public void addOperator(Operator op)
    {
        operators.add(op);
    }


    public ArrayList<Float> getOperands()
    {   
        return operands;
    }


    public ArrayList<Operator> getOperators()
    {
        return operators;
    }






}