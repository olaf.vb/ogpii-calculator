import javax.swing.*;
import java.awt.GridLayout;

public class Calculator
{
    JFrame frame;
    CalcModel calcModel;
    CalcViewButtons calcViewButtons;
    CalcViewDisplay calcViewDisplay;

    public Calculator()
    {
        try 
        {
            calcModel = new CalcModel();
            calcViewButtons = new CalcViewButtons(calcModel, null);
            calcViewDisplay = new CalcViewDisplay(calcModel, null);

            calcModel.addObserver(calcViewDisplay);

            createGUI();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException
    {
        UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        Calculator calc = new Calculator();
    }


    private void createGUI()
    {
        JFrame frame = new JFrame("Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2, 1));
        panel.add(calcViewDisplay.getDisplay());
        panel.add(calcViewButtons.getUI());
        frame.getContentPane().add(panel);

        frame.pack();
        frame.setVisible(true);
    }



}