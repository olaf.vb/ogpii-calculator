import java.util.Observable;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import MVC.*;

public class CalcViewDisplay extends AbstractView
{
    JTextField display;

    public CalcViewDisplay(Observable model, Controller controller)
    {
        super(model, controller);
        display = new JTextField();
        
        
        // display.addActionListener(new ActionListener() 
        // {

        //     @Override
        //     public void actionPerformed(ActionEvent e) 
        //     {
        //         ((CalcController)getController()).setString(display.getText());
        //     }
        // }
        // );

        display.getDocument().addDocumentListener(new DocumentListener() 
        {
            public void changedUpdate(DocumentEvent e) 
            {
                ((CalcController)getController()).updateString(display.getText());
            }
            public void removeUpdate(DocumentEvent e) 
            {
                ((CalcController)getController()).updateString(display.getText());
            }
            public void insertUpdate(DocumentEvent e) 
            {
                ((CalcController)getController()).updateString(display.getText());
            }
        });


    }


    public void update(Observable o, Object info)
    {
        display.setText((String) info);
    }


    public JTextField getDisplay()
    {
        return display;
    }

    @Override
    public Controller defaultController(Observable model) {
        return new CalcController(model);
    }



}