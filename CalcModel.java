import java.nio.charset.MalformedInputException;
import java.util.Observable;

public class CalcModel extends Observable
{
    float last;
    String expression;


    public CalcModel()
    {   
        expression = "";
    }   


    public void setExpression(String s)
    {
        expression = s;
        setChanged();
        notifyObservers(expression);
    }

    public void silentChangeExpression(String s)
    {
        expression = s;
    }

    
    public void addToExpression(String s)
    {
        expression += s;
        setChanged();
        notifyObservers(expression);
    }

    public void calculate() throws Exception
    {
        last = calcFromExpression(CalcModelParser.parseExpression(expression));
        setChanged();
        notifyObservers("" + last);
    }

    private float calcFromExpression(CalcExpression expr) throws Exception
    {
        try
        {
            float result = expr.getOperands().get(0);

            for(int i = 1; i < expr.getOperands().size(); ++i)
            {
                result = operation(result, expr.getOperands().get(i), expr.getOperators().get(i - 1));
            }

            return result;
        }
        catch (Exception e)
        {
            throw e;
        }



    }


    private float operation(float a, float b, CalcExpression.Operator op) throws MalformedInputException {
        switch (op)   {
            case ADDITION: return a + b;
            case SUBTRACTION: return a - b;
            case MULTIPLICATION: return a * b;
            case DIVISION: return a / b;
            default: throw new MalformedInputException(2);
        }
    }







}