import java.util.Observable;

import MVC.*;

public class CalcController extends AbstractController
{
    public CalcController(Observable model)
    {
        super(model);
        

    }

    public void updateString(String s)
    {
        ((CalcModel) getModel()).silentChangeExpression(s);
    }


    public void addString(String s)
    {
        ((CalcModel) getModel()).addToExpression(s);
    }

    public void setString(String s)
    {
        ((CalcModel) getModel()).setExpression(s);
    }

    public void result()
    {
        try
        {
            ((CalcModel) getModel()).calculate();
        }
        catch (Exception e)
        {

        }
        
    }



}