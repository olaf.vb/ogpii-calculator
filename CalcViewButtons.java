import java.util.Observable;

import javax.swing.*;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import MVC.*;

public class CalcViewButtons extends AbstractView
{
    JPanel panel;


    public CalcViewButtons(Observable model, Controller controller)
    {
        super(model, controller);
        panel = new JPanel();
        panel.setLayout(new GridLayout(5,4));

        addButtons();
    }


    private void addButtons()
    {
        // add numeric buttons
        String [] key = {"7","8","9","+","4","5","6","-","1","2","3","/","0",".", "*"};
        for(int i = 0 ; i < key.length;i++)
        {
            JButton button = new JButton(key[i]);
            String temp = key[i];
            button.addActionListener(new ActionListener() 
            {
    
                @Override
                public void actionPerformed(ActionEvent e) 
                {
                    
                    ((CalcController)getController()).addString(temp);
                }
            }
            );

            panel.add(button);
        }

        addResultButton();
        addClearButton();
        
    }


    private void addResultButton()
    {
        JButton result = new JButton("=");
        result.addActionListener(new ActionListener() 
        {

            @Override
            public void actionPerformed(ActionEvent e) 
            {
                
                ((CalcController)getController()).result();
            }
        }
        );
        panel.add(result);
    }

    private void addClearButton()
    {
        JButton result = new JButton("clear");
        result.addActionListener(new ActionListener() 
        {

            @Override
            public void actionPerformed(ActionEvent e) 
            {
                
                ((CalcController)getController()).setString("");
            }
        }
        );

        panel.add(result);
    }




    @Override
    public Controller defaultController(Observable model) {
        return new CalcController(model);
    }


    public JPanel getUI()
    {
        return panel;
    }


}