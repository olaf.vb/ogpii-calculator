
public class CalcModelParser
{
    /**
     * Parses a given string to a calcexpression, does NOT take into account order of operators
     * @param inp: string 
     * @return CalcExpression: the parsed CalcExpression from the given string
     */
    public static CalcExpression parseExpression(String inp)
    {   
        CalcExpression expr = new CalcExpression();
        inp.replaceAll("\\s","");

        String[] inpArray = inp.split("(?<=[-+*/])|(?=[-+*/])");
        


        for(String s: inpArray)
        {
            if(isOperator(s))
                expr.addOperator(CalcExpression.Operator.fromString(s));
            else
                expr.addOperand(Float.parseFloat(s));
        }   

        
        return expr;
    }

    /**
     * Checks wether the giving paramter is exactly an operator (+, -, * or /)
     * @param s: A string 
     * @return true if string is an operator, false if string is not an operator
     */
    private static boolean isOperator(String s)
    {
        for(CalcExpression.Operator op: CalcExpression.Operator.values() )
        {
            if(op.toString().equals(s))
                return true;
        }
        return false;
    }



}